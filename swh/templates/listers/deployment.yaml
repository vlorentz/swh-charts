{{ if .Values.listers.enabled -}}
{{- $configurationChecksum := include (print $.Template.BasePath "/listers/configmap.yaml") . -}}
{{- range $lister_type, $deployment_config := .Values.listers.deployments -}}
{{- $lister_name := ( print "lister-" $lister_type ) -}}
# Set useJsonLogger to false to let the logs be plain text
{{- $use_json_logger := get $deployment_config "useJsonLogger" | default true }}
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ $lister_name }}
  namespace: {{ $.Values.namespace }}
  labels:
    app: {{ $lister_name }}
spec:
  revisionHistoryLimit: 2
  selector:
    matchLabels:
      app: {{ $lister_name }}
  strategy:
    type: RollingUpdate
    rollingUpdate:
      maxSurge: 1
  template:
    metadata:
      labels:
        app: {{ $lister_name }}
      annotations:
        # Force a rollout upgrade if the configuration changes
        checksum/config: {{ $configurationChecksum | sha256sum }}
    spec:
      {{- if $.Values.listers.affinity }}
      affinity:
        {{ toYaml $.Values.listers.affinity | nindent 8 }}
      {{- end }}
      initContainers:
        - name: prepare-configuration
          image: debian:bullseye
          imagePullPolicy: Always
          env:
          - name: AMQP_USERNAME
            valueFrom:
              secretKeyRef:
                name: common-secrets
                key: rabbitmq-amqp-username
                # 'name' secret must exist & include that ^ key
                optional: false
          - name: AMQP_PASSWORD
            valueFrom:
              secretKeyRef:
                name: common-secrets
                key: rabbitmq-amqp-password
                # 'name' secret must exist & include that ^ key
                optional: false
          command:
            - /entrypoint.sh
          volumeMounts:
          - name: configuration-template
            mountPath: /entrypoint.sh
            subPath: "init-container-entrypoint.sh"
            readOnly: true
          - name: configuration
            mountPath: /etc/swh
          - name: configuration-template
            mountPath: /etc/swh/configuration-template
          - name: lister-credentials-secrets
            mountPath: /etc/credentials/listers
            readOnly: true
          - name: sentry-settings-for-celery-tasks
            mountPath: /etc/credentials/sentry-settings
            readOnly: true
      containers:
      - name: listers
        resources:
          requests:
            memory: {{ get $deployment_config "requestedMemory" | default "256Mi" }}
            cpu: {{ get $deployment_config "requestedCpu" | default "250m" }}
        image: {{ $.Values.swh_lister_image }}:{{ $.Values.swh_lister_image_version }}
        imagePullPolicy: Always
        command:
        - /bin/bash
        args:
        - -c
        - /opt/swh/entrypoint.sh
        lifecycle:
          preStop:
            exec:
              command: ["/pre-stop.sh"]
        env:
        - name: STATSD_HOST
          value: {{ $.Values.statsdExternalHost | default "prometheus-statsd-exporter" }}
        - name: STATSD_PORT
          value: {{ $.Values.statsdPort | default "9125" | quote }}
        - name: MAX_TASKS_PER_CHILD
          value: {{ get $deployment_config "maxTasksPerChild" | default 1 | quote }}
        - name: LOGLEVEL
          value: {{ get $deployment_config "logLevel" | default "INFO" | quote }}
        - name: SWH_CONFIG_FILENAME
          value: /etc/swh/config.yml
        {{ if $use_json_logger }}
        - name: SWH_LOG_CONFIG
          value: /etc/swh/logging-configuration.yml
        {{ end }}
        - name: SWH_SENTRY_ENVIRONMENT
          value: {{ $.Values.sentry.environment }}
        volumeMounts:
          - name: lister-utils
            mountPath: /pre-stop.sh
            subPath: "pre-stop.sh"
          - name: configuration
            mountPath: /etc/swh
        {{ if $use_json_logger }}
          - name: configuration-template
            mountPath: /etc/swh/logging-configuration.yml
            subPath: "logging-configuration.yml"
            readOnly: true
        {{ end }}
      volumes:
      - name: configuration
{{ toYaml ($.Values.tmpEphemeralStorage.claimTemplate | default $.Values.tmpEphemeralStorage.default) | indent 8 }}
      - name: configuration-template
        configMap:
          name: {{ $lister_name }}-template
          defaultMode: 0777
          items:
          - key: "config.yml.template"
            path: "config.yml.template"
          - key: "init-container-entrypoint.sh"
            path: "init-container-entrypoint.sh"
        {{ if $use_json_logger }}
          - key: "logging-configuration.yml"
            path: "logging-configuration.yml"
        {{ end }}
      - name: lister-utils
        configMap:
          name: lister-utils
          defaultMode: 0777
          items:
          - key: "pre-stop-idempotent.sh"
            path: "pre-stop.sh"
      - name: lister-credentials-secrets
        secret:
          secretName: lister-credentials-secrets
          optional: true
      - name: sentry-settings-for-celery-tasks
        secret:
          secretName: sentry-settings-for-celery-tasks
          optional: true
{{ end }}
{{- end -}}
